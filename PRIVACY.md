This app does not collect any personal data.

If you installed this app from an app store, such as the Google Play Store,
then refer to that app store's privacy policy for any information they collect.