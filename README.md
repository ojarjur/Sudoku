# A minimalistic Sudoku app

This project is a minimalistic Sudoku game.

The core game is written as a single, static HTML file, and then
packaged as an Android app.

The goal of this app is to eliminate all extraneous features and
focus solely on the core game experience.

It contains none of the extra fluff that is found in so many apps today:

* No adds.
* No in-app purchases.
* No data harvesting.
* No user tracking.
* No gamification tricks to increase engagement.

This is Sudoku, and *just* Sudoku.