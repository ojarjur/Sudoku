/**
Copyright (C) 2024 Omar Jarjur. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.jarjur.sudoku

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.webkit.JsResult
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import androidx.activity.ComponentActivity
import androidx.webkit.WebViewAssetLoader
import androidx.webkit.WebViewClientCompat

private class StaticAssetsClient(private val assetLoader: WebViewAssetLoader) : WebViewClientCompat() {
    override fun shouldInterceptRequest(
        view: WebView,
        request: WebResourceRequest
    ): WebResourceResponse? {
        return assetLoader.shouldInterceptRequest(request.url)
    }
}

private class ConfirmWebChromeClient(private val context: android.content.Context) : WebChromeClient() {
    override fun onJsConfirm(
        view: WebView?,
        url: String?,
        message: String?,
        result: JsResult?
    ): Boolean {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder
            .setMessage(message)
            .setTitle("Sudoku")
            .setPositiveButton("OK") { _, _ ->
                result?.confirm()
            }
            .setNegativeButton("Cancel") { _, _ ->
                result?.cancel()
            }

        val dialog: AlertDialog = builder.create()
        dialog.show()
        return true
    }

    override fun onJsAlert(
        view: WebView?,
        url: String?,
        message: String?,
        result: JsResult?
    ): Boolean {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder
            .setMessage(message)
            .setTitle("Sudoku")
            .setPositiveButton("OK") { _, _ ->
                result?.confirm()
            }

        val dialog: AlertDialog = builder.create()
        dialog.show()
        return true
    }
}

class MainActivity : ComponentActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //enableEdgeToEdge()
        val assetLoader = WebViewAssetLoader.Builder()
            .addPathHandler("/assets/", WebViewAssetLoader.AssetsPathHandler(this))
            .addPathHandler("/res/", WebViewAssetLoader.ResourcesPathHandler(this))
            .build()
        val sudokuWebView = WebView(this)
        sudokuWebView.settings.javaScriptEnabled = true
        sudokuWebView.settings.domStorageEnabled = true
        sudokuWebView.webViewClient = StaticAssetsClient(assetLoader)
        sudokuWebView.setWebChromeClient(ConfirmWebChromeClient(this))
        sudokuWebView.loadUrl("https://appassets.androidplatform.net/assets/sudoku.html")
        setContentView(sudokuWebView)
    }
}