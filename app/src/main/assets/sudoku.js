/*
  Copyright (C) 2024 Omar Jarjur. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

var localStorageKey = "sudokuGameState";

// Our protoype boards.
//
// The following array holds three entries each consisting of a starting board and a set
// of matching game masks.
//
// Each number in one of the masks represents a single cell in the starting board that is
// revealed at the start of the game.
//
// Each such game is maximally difficult: not a single remaining space can be cleared out
// without making the solution ambiguous.
//
// We create a random game by picking a starting board, applying one of the masks to it,
// and then shuffling the result.
var startingBoards = [
    [[[6, 3, 5, 2, 7, 8, 1, 9, 4],
      [1, 4, 7, 3, 5, 9, 2, 6, 8],
      [9, 2, 8, 6, 4, 1, 5, 7, 3],
      [5, 9, 2, 7, 8, 3, 4, 1, 6],
      [7, 8, 6, 4, 1, 2, 3, 5, 9],
      [3, 1, 4, 5, 9, 6, 8, 2, 7],
      [2, 7, 3, 8, 6, 5, 9, 4, 1],
      [4, 5, 9, 1, 3, 7, 6, 8, 2],
      [8, 6, 1, 9, 2, 4, 7, 3, 5]],
     [[[4, 5, 6], [3, 6, 7], [5, 6], [1, 2], [2, 3], [0, 2], [0, 2, 4, 7], [1, 5, 7], [3, 7]],
      [[3, 6, 8], [2, 5], [1, 3], [4, 7], [2], [2, 5, 6, 8], [0, 5], [1, 5, 6], [0, 2, 4, 7]],
      [[4], [0, 8], [1, 3, 6, 8], [], [0, 1, 5, 6, 7], [2, 4, 8], [1, 2, 7], [1, 3, 6, 7], [1]],
      [[0, 2, 3, 7], [3, 7], [1, 5], [1], [2, 6, 7], [2, 3, 4, 7], [1, 3, 4, 7], [3, 4], [6]],
      [[0, 4, 5, 7], [5, 8], [1, 5, 8], [4, 5], [3], [1, 3, 6], [7], [2, 5, 6], [0, 1, 2, 7]],
      [[5], [0, 1, 2, 3, 8], [3, 7], [0, 2, 4], [4, 5, 6, 8], [4, 6], [1, 6, 8], [0], [2, 5]],
      [[2, 5, 6], [0, 3, 6], [4, 7], [1, 2, 4], [3, 5], [0, 4], [1, 4], [1, 3, 8], [1, 3, 8]],
      [[1, 3], [2], [0, 4, 6], [1, 2, 3, 5, 8], [], [2, 6, 7], [4, 6, 7], [1, 3, 7], [0, 1, 6]],
      [[2, 6], [1, 4, 5, 7], [1, 7, 8], [2, 3], [5], [4, 5], [3, 4, 7], [1, 2, 4, 8], [1, 2]],
      [[1, 2, 5], [0, 2], [0, 1], [3], [1, 4, 7], [4, 7, 8], [3, 6, 8], [0, 1, 5, 8], [3, 7]]]],
    [[[6, 4, 9, 2, 8, 1, 3, 7, 5],
      [8, 7, 3, 9, 5, 6, 2, 4, 1],
      [2, 1, 5, 4, 7, 3, 6, 9, 8],
      [3, 5, 8, 1, 6, 4, 9, 2, 7],
      [7, 9, 6, 3, 2, 5, 1, 8, 4],
      [4, 2, 1, 7, 9, 8, 5, 3, 6],
      [5, 8, 7, 6, 3, 2, 4, 1, 9],
      [9, 3, 4, 5, 1, 7, 8, 6, 2],
      [1, 6, 2, 8, 4, 9, 7, 5, 3]],
     [[[4, 8], [1, 2], [0, 1, 5, 7], [0, 2, 4, 6], [2], [1, 7], [6, 7], [6], [1, 4, 5, 6]],
      [[5, 7, 8], [0, 6, 7], [3, 4], [1], [2, 3, 4], [0, 8], [5, 6], [1, 4], [1, 2, 5, 6, 7]],
      [[0, 1, 3, 8], [0, 2, 7], [5], [2], [4, 5], [2, 7, 8], [0, 2, 8], [1, 4, 8], [3, 4]],
      [[0, 3, 7], [0, 1, 5], [3, 5], [6, 8], [3, 7], [2, 4, 6], [1, 3, 5, 7], [3, 7], [2, 6]],
      [[1, 4, 5, 8], [6], [6, 7], [2, 5, 6], [0, 2, 5], [1, 2, 8], [2, 3, 7], [5], [2, 5, 6]],
      [[3], [0, 2, 7, 8], [1, 4], [4, 5], [0, 2, 5], [1, 4, 6, 7], [1, 7], [0, 2, 6, 8], [6]],
      [[0, 2, 7], [2, 7], [1, 2, 6], [1, 3, 4], [3, 6], [0, 1, 7], [1, 7], [5, 6], [4, 5]],
      [[0, 5], [6, 7], [4, 8], [0, 4, 5, 7], [3, 4], [1, 2, 6, 8], [2, 8], [3, 6], [2, 3, 7]],
      [[2, 5], [1, 5, 8], [0, 2, 7], [2, 4, 8], [6], [0, 4, 8], [5, 6], [0, 2, 3], [3, 7]],
      [[0, 1, 6, 8], [2, 3], [0, 4, 5], [1, 5], [0, 2, 4, 6], [], [3, 5, 7, 8], [1, 3], [0, 6]]]],
    [[[9, 2, 8, 1, 7, 5, 6, 3, 4],
      [4, 6, 3, 8, 2, 9, 7, 5, 1],
      [5, 7, 1, 6, 4, 3, 2, 9, 8],
      [8, 3, 9, 5, 6, 2, 4, 1, 7],
      [1, 4, 6, 7, 3, 8, 5, 2, 9],
      [2, 5, 7, 4, 9, 1, 3, 8, 6],
      [3, 9, 4, 2, 8, 6, 1, 7, 5],
      [6, 1, 2, 9, 5, 7, 8, 4, 3],
      [7, 8, 5, 3, 1, 4, 9, 6, 2]],
     [[[0, 4], [1, 4, 7], [1, 6, 8], [3, 4, 8], [0, 4, 8], [6, 7], [0, 6], [0, 2, 3, 8], [5]],
      [[3, 6, 8], [1, 5], [1, 4, 5, 8], [0, 8], [6, 8], [3, 5], [1, 2, 4, 6], [4], [2, 3, 7]],
      [[1, 7, 8], [3, 5], [2, 3], [0, 2, 3, 4], [1, 7], [4], [2, 7], [5, 6, 8], [2, 3, 6]],
      [[0, 8], [2, 4, 7], [1, 2, 5], [3, 4, 7], [1, 3, 8], [0], [], [3, 4, 7], [1, 2, 7]],
      [[3], [0, 4, 6, 8], [0, 5, 8], [2, 6], [2, 3, 7], [0, 1, 3, 7], [5, 6], [4, 7], [6, 7]],
      [[4, 7], [0, 1, 3], [0, 6, 7], [2, 3, 4], [7], [1, 2, 5, 6], [4, 6, 7, 8], [0, 6], [4]],
      [[4, 8], [2, 8], [3, 5, 6, 7], [0, 3, 5], [1, 5], [1, 6, 8], [3, 6, 8], [1, 2], [5, 7]],
      [[4, 6, 8], [2, 7, 8], [0, 3, 6], [0, 8], [0], [1, 3, 4, 7], [2, 3, 4], [1, 5], [7, 8]],
      [[5], [2, 4, 6], [2, 4], [2, 6, 7], [1, 2, 4, 6], [0, 2, 8], [], [3, 5, 8], [1, 3, 5, 8]],
      [[1, 2, 5], [0, 3, 8], [5], [3, 4, 6], [0, 2, 7, 8], [7], [5], [0, 3, 5, 8], [1, 2, 4]]]],
    [[[5, 6, 4, 1, 8, 2, 3, 9, 7],
      [9, 3, 7, 4, 6, 5, 1, 8, 2],
      [1, 8, 2, 7, 3, 9, 5, 4, 6],
      [4, 2, 5, 9, 1, 3, 7, 6, 8],
      [3, 1, 9, 8, 7, 6, 2, 5, 4],
      [6, 7, 8, 5, 2, 4, 9, 3, 1],
      [8, 9, 3, 2, 4, 1, 6, 7, 5],
      [7, 5, 1, 6, 9, 8, 4, 2, 3],
      [2, 4, 6, 3, 5, 7, 8, 1, 9]],
     [[[3, 4], [5, 7], [0, 2], [0, 3], [6, 7], [1, 4, 7], [1, 2, 4, 6], [1, 5], [2, 5, 7]],
      [[2, 3, 7], [8], [0, 1, 3], [0, 6, 7], [0, 3], [2, 4, 5], [1, 8], [0, 4, 6, 8], [5, 6]],
      [[5, 7], [2, 4, 6], [5, 7, 8], [0, 2, 4, 6], [4, 6, 7], [0, 5], [1, 6], [5], [1, 6, 7]],
      [[2, 3, 4, 5, 7], [1, 5], [4, 6, 8], [5, 6], [2, 4, 6], [2], [0, 4], [1, 5, 7], [1, 7]],
      [[2, 5], [2, 6], [1, 3, 6], [0, 3, 6], [7], [3, 4, 7, 8], [1], [1, 3, 7, 8], [2, 4, 5]],
      [[1, 5], [6], [0, 1, 5], [0, 2, 4, 7], [2, 6, 8], [0, 3, 6], [4, 6], [5, 7], [3, 5, 8]],
      [[2, 4, 6], [0, 7, 8], [2, 6], [1, 5, 8], [1, 5], [0, 3], [2, 3, 7], [1, 2, 6], [5, 8]],
      [[1, 7, 8], [3, 5, 7], [2, 6], [0, 2, 5], [1, 2, 4], [3], [1, 4, 5, 7], [3, 4], [0, 6]],
      [[0, 6], [3, 4, 6], [2, 8], [0, 1, 2, 3], [3, 4, 5], [], [0, 1, 5, 7], [5, 7, 8], [1]],
      [[2, 5, 8], [1, 7], [6, 7, 8], [0, 1, 6], [1, 3, 7], [3, 6, 7], [0, 4], [0, 2], [5, 8]]]],
    [[[5, 9, 3, 8, 7, 1, 2, 6, 4],
      [4, 8, 6, 2, 5, 3, 7, 9, 1],
      [1, 7, 2, 4, 9, 6, 5, 3, 8],
      [6, 4, 5, 7, 3, 2, 8, 1, 9],
      [7, 1, 9, 5, 4, 8, 6, 2, 3],
      [2, 3, 8, 1, 6, 9, 4, 7, 5],
      [3, 5, 7, 6, 1, 4, 9, 8, 2],
      [9, 2, 4, 3, 8, 7, 1, 5, 6],
      [8, 6, 1, 9, 2, 5, 3, 4, 7]],
     [[[3, 5], [7], [0, 1, 3, 5, 6], [5, 6, 8], [2, 7], [1, 4, 7], [0, 1, 2], [6, 8], [0, 2]],
      [[0, 1, 3, 6, 8], [3, 5, 6], [0], [1, 7, 8], [2, 8], [0, 4, 8], [5], [1, 5, 7], [2, 3]],
      [[7, 8], [0, 3], [1, 2, 5, 6], [3, 5, 8], [0, 5, 8], [6], [1, 4], [0, 1, 2], [0, 1, 5]],
      [[5, 6], [1, 3, 7], [1, 5, 8], [0], [7, 8], [2, 4, 7, 8], [1, 2], [4, 6, 8], [3, 4, 6]],
      [[1, 6, 8], [1], [0, 4, 5], [0, 4, 6, 8], [0, 1, 2, 6], [3], [0, 1, 8], [4, 5, 6, 8], []],
      [[1, 6], [1, 4, 8], [0, 3, 5], [2], [0, 5], [3, 5, 6], [5, 7], [0, 3, 7, 8], [1, 4]],
      [[], [2, 6], [0, 2, 3, 4, 6], [2, 5, 6, 7], [2, 7], [1, 6], [3, 5, 8], [0, 3, 8], [0, 5]],
      [[5, 8], [1, 7], [1, 3, 5, 6], [6], [0, 5, 7], [3, 4], [1, 5], [0, 1, 3, 8], [0, 2, 5]],
      [[2], [8], [2, 3, 4, 6], [0, 4], [0, 1, 7], [0, 5, 6, 7], [3, 4], [5, 7], [6, 7, 8]],
      [[0, 2, 6], [1, 8], [3, 4], [5], [0, 2, 8], [0, 2, 3, 6], [2, 3], [0, 4, 6, 7], [5, 7]]]],
    [[[7, 3, 9, 6, 2, 1, 8, 5, 4],
      [8, 2, 1, 9, 4, 5, 7, 3, 6],
      [4, 6, 5, 8, 7, 3, 2, 9, 1],
      [3, 9, 7, 5, 6, 2, 1, 4, 8],
      [6, 1, 4, 7, 3, 8, 9, 2, 5],
      [5, 8, 2, 4, 1, 9, 6, 7, 3],
      [9, 5, 3, 1, 8, 7, 4, 6, 2],
      [1, 7, 6, 2, 5, 4, 3, 8, 9],
      [2, 4, 8, 3, 9, 6, 5, 1, 7]],
     [[[1, 2], [0, 1, 3, 4, 7], [0], [2, 4, 6], [0, 5, 7], [2, 7, 8], [], [2, 3, 7], [3, 5, 6]],
      [[3, 4, 6], [2, 6, 7], [0, 6, 7], [2, 4], [0, 5, 6, 8], [1, 3], [0, 1], [5, 6], [3, 8]],
      [[1, 7], [1, 4, 6], [1, 2, 3, 4], [4, 7, 8], [6], [1, 3], [0, 2, 6, 7], [0], [3, 7]],
      [[0, 2, 8], [5, 7], [5, 6], [2, 3, 8], [0, 2, 3, 6, 8], [2, 3, 6], [1], [0], [4, 5, 7]],
      [[3], [1, 3, 6], [2, 3, 4, 8], [1, 4, 5], [2, 5, 6], [8], [0, 7], [0, 5, 7], [1, 2, 7]],
      [[7], [2, 4, 6], [3, 5, 6], [2, 4, 5, 6, 8], [2], [0, 1], [0], [2, 6, 8], [1, 5, 8]],
      [[2, 8], [0, 5, 8], [0, 4, 5, 6, 8], [3], [0, 1], [1, 6, 7], [1, 3, 6, 8], [3, 8], [4]],
      [[2, 3, 8], [5, 6], [4, 7], [7, 8], [0, 2, 6, 8], [4], [0, 1], [0, 5, 6], [0, 2, 5, 7]],
      [[1, 2, 8], [0, 8], [1, 7, 8], [1, 4, 5], [2, 5, 6, 7], [], [4], [4, 6, 8], [0, 2, 5, 6]],
      [[1, 2, 5], [4, 6], [1, 4, 5], [2, 4, 5], [1, 6, 8], [0, 3, 6], [0, 6], [3], [2, 6, 8]]]],
    [[[8, 3, 5, 9, 1, 4, 6, 2, 7],
      [2, 4, 7, 5, 3, 6, 9, 1, 8],
      [9, 6, 1, 2, 7, 8, 5, 3, 4],
      [6, 2, 4, 3, 5, 9, 7, 8, 1],
      [7, 1, 9, 6, 8, 2, 3, 4, 5],
      [3, 5, 8, 7, 4, 1, 2, 9, 6],
      [5, 8, 2, 4, 9, 7, 1, 6, 3],
      [4, 7, 6, 1, 2, 3, 8, 5, 9],
      [1, 9, 3, 8, 6, 5, 4, 7, 2]],
     [[[6, 8], [1, 4, 6], [2, 4, 5], [8], [2, 3, 6, 8], [1, 4], [2, 3, 4, 5], [2], [0, 2, 8]],
      [[0, 6], [4, 6], [0, 5], [0, 1, 3, 7], [4, 5, 7, 8], [4], [0, 1, 5, 7], [1, 4], [6, 8]],
      [[8], [0, 3, 5, 7], [0, 3], [0, 5], [4, 6], [0, 2, 4, 6], [2, 3], [1, 6, 7, 8], [2]],
      [[0, 3], [8], [0, 3, 7, 8], [1, 2, 3], [3], [2, 5, 6], [2, 5, 6], [2], [2, 4, 5, 6, 7]],
      [[2, 4, 5], [2, 5, 6, 8], [5, 7], [4, 5], [2, 6, 7], [0, 3, 6], [6], [2], [1, 3, 7, 8]],
      [[1, 8], [3, 5, 7, 8], [3, 4, 6], [0, 5], [1], [2, 3, 5, 6], [7, 8], [1, 3, 4], [3, 6]],
      [[2, 4, 7], [0, 1], [4], [4, 6, 8], [2, 8], [0, 2, 4], [], [0, 1, 5, 8], [3, 5, 8]],
      [[4, 5, 6, 8], [1, 6], [2, 3, 6, 7], [2, 4, 7], [3, 4, 8], [0], [0, 1, 4, 8], [3], [4]],
      [[2, 7], [7, 8], [0, 4], [1, 3, 7], [0, 2, 8], [5, 8], [0, 1, 3, 6], [0], [2, 4, 5, 6]],
      [[4], [2, 8], [0, 1, 4, 8], [2, 4], [3, 5, 6], [1, 2, 6, 8], [6], [3, 5], [2, 7]]]],
    [[[7, 4, 2, 5, 9, 6, 8, 3, 1],
      [3, 5, 9, 4, 1, 8, 2, 6, 7],
      [1, 6, 8, 2, 3, 7, 9, 4, 5],
      [6, 9, 7, 3, 5, 1, 4, 2, 8],
      [4, 2, 3, 7, 8, 9, 1, 5, 6],
      [5, 8, 1, 6, 2, 4, 7, 9, 3],
      [2, 7, 5, 1, 4, 3, 6, 8, 9],
      [8, 1, 4, 9, 6, 5, 3, 7, 2],
      [9, 3, 6, 8, 7, 2, 5, 1, 4]],
     [[[0, 4, 5], [2, 3, 4, 5, 8], [8], [6, 7], [2, 3], [0, 2, 4, 7], [3, 5], [0], [0, 6]],
      [[4, 5, 6], [1, 2, 4], [7], [0, 3], [0, 3], [1, 4, 6], [0, 1], [0, 6, 8], [2, 7, 8]],
      [[4, 6, 7], [1, 6], [0, 1, 5], [0, 6, 7], [1, 3, 5, 7, 8], [4], [2, 3, 8], [0, 1, 2], []],
      [[0, 2, 6, 7], [3, 7], [0, 2], [1], [2, 4, 5, 6], [2, 4, 6], [2, 5, 6, 7], [3, 5], [4]],
      [[4, 6], [3, 7], [0, 1, 3, 5], [3, 6, 8], [4], [0, 2, 4], [1, 4, 8], [2, 3, 6], [2, 7]],
      [[1, 2], [0, 6, 8], [1, 5, 6], [1, 3, 6], [1, 4, 6], [2, 7], [4, 6], [0, 4, 7], [1, 3]],
      [[0, 6], [1, 4, 7], [2, 3, 4], [0, 3, 6, 7], [1, 2, 8], [4, 7], [1], [2, 3, 6], [3, 6]],
      [[8], [3, 7, 8], [0, 2, 3], [2, 4, 6], [2, 5], [4, 7], [1, 2, 5], [0, 2, 5], [1, 3, 7]],
      [[3, 4, 8], [2, 6, 8], [0], [3, 5], [0, 1, 8], [3], [0, 1, 4, 7], [6], [0, 4, 5, 6]],
      [[5, 6], [1, 5, 6, 7, 8], [0], [2, 7], [0], [1, 3, 5, 7], [4, 5], [1], [0, 1, 3, 6, 8]]]],
    [[[5, 7, 2, 8, 4, 1, 3, 9, 6],
      [3, 6, 8, 2, 9, 5, 1, 4, 7],
      [4, 1, 9, 6, 3, 7, 2, 5, 8],
      [8, 3, 4, 5, 2, 9, 6, 7, 1],
      [1, 5, 7, 4, 6, 8, 9, 3, 2],
      [2, 9, 6, 1, 7, 3, 5, 8, 4],
      [6, 4, 5, 3, 8, 2, 7, 1, 9],
      [9, 8, 1, 7, 5, 6, 4, 2, 3],
      [7, 2, 3, 9, 1, 4, 8, 6, 5]],
     [[[5], [1, 3, 4], [0, 2, 3, 5], [1, 7, 8], [3, 4, 6], [7], [0, 1], [3, 4, 6, 8], [2, 8]],
      [[0, 4], [1, 2], [1, 4, 7, 8], [4], [3, 5, 7], [0, 4, 5, 6], [1, 7], [1, 5, 8], [0, 3]],
      [[3, 6, 7], [6, 8], [1], [0, 1, 5], [2, 3, 8], [1, 5, 8], [0, 2, 8], [3, 5], [1, 4, 8]],
      [[5, 6, 7], [1, 3, 5], [5, 8], [0, 1, 6], [3, 7], [4, 6], [0, 1, 5, 6], [0, 4], [6, 8]],
      [[1, 2, 5], [7], [2, 3, 5], [0, 1, 3, 8], [2, 6, 7, 8], [], [5, 8], [3, 8], [6, 7, 8]],
      [[2, 7], [1, 5], [1, 3], [0, 7, 8], [1, 4, 8], [1, 5, 6, 8], [4, 5], [6], [2, 3, 4, 7]],
      [[0, 2, 7], [0, 5, 8], [1, 8], [5, 7], [3, 8], [2, 5, 6], [1, 4], [3, 8], [1, 3, 4, 6]],
      [[0, 1, 5, 7], [3, 6], [0, 3, 4], [1, 3], [3, 4], [6, 7], [5, 6, 7], [1, 2, 8], [3, 8]],
      [[7, 8], [3, 6], [1, 4, 8], [1, 3, 7], [0, 2, 5, 6, 7], [], [2, 3], [1, 5], [2, 3, 5, 7]],
      [[3, 8], [5, 7], [1, 4, 6], [1, 7, 8], [1, 4, 7], [0, 2, 5], [7], [3], [1, 2, 5, 6, 8]]]],
    [[[5, 6, 1, 4, 7, 2, 9, 8, 3],
      [8, 3, 4, 6, 9, 1, 2, 7, 5],
      [2, 9, 7, 3, 5, 8, 4, 1, 6],
      [9, 1, 3, 2, 4, 6, 8, 5, 7],
      [6, 7, 5, 1, 8, 9, 3, 4, 2],
      [4, 8, 2, 5, 3, 7, 6, 9, 1],
      [3, 4, 8, 7, 6, 5, 1, 2, 9],
      [7, 2, 6, 9, 1, 4, 5, 3, 8],
      [1, 5, 9, 8, 2, 3, 7, 6, 4]],
     [[[1, 3, 6], [0], [0, 3, 8], [2, 3, 6, 7], [0, 4, 5], [3], [6], [0, 4, 5, 8], [1, 4, 8]],
      [[0, 2, 6, 8], [3, 6], [0, 3], [7], [1, 5, 6], [1, 3, 4], [2, 5, 8], [1, 5], [3, 7, 8]],
      [[4, 7], [1, 4, 5], [0, 3, 6], [4, 5, 8], [], [1, 8], [2, 5, 7], [1, 6, 7], [0, 2, 4]],
      [[3], [6], [0, 1, 3], [2, 7], [0, 2, 4, 8], [2, 5, 7, 8], [0, 2, 4, 6], [3, 8], [1, 6]],
      [[2, 3, 7], [1, 6, 8], [], [3], [1, 5, 6], [0, 1, 4, 6, 8], [3, 7], [2, 3, 4], [0, 1, 5]],
      [[2, 3, 6], [7, 8], [4, 8], [0, 4], [4, 8], [0, 6], [1, 3, 6], [1, 3, 7, 8], [1, 5, 7]],
      [[2], [1], [4, 5, 6], [4, 7], [5, 8], [2, 4, 6, 8], [1, 4, 5, 8], [0, 3, 7, 8], [0, 1]],
      [[7], [2, 4], [2, 3], [0, 1, 5, 7], [1, 3, 7], [2, 5, 6, 8], [1, 2, 5, 6], [5], [4, 6]],
      [[0, 2, 3], [0, 3, 6, 7], [3, 8], [0, 2, 6], [1], [5, 7, 8], [2, 6], [4, 7], [1, 2, 4]],
      [[6], [0, 3, 4], [1, 2, 4], [2, 8], [0, 1, 4, 8], [0, 3, 8], [7], [5, 8], [1, 2, 4, 5]]]],
    [[[1, 9, 4, 5, 6, 8, 3, 2, 7],
      [2, 5, 8, 9, 3, 7, 1, 4, 6],
      [3, 7, 6, 1, 4, 2, 5, 9, 8],
      [4, 1, 9, 2, 8, 5, 7, 6, 3],
      [5, 2, 3, 6, 7, 1, 4, 8, 9],
      [6, 8, 7, 4, 9, 3, 2, 5, 1],
      [7, 4, 1, 8, 2, 6, 9, 3, 5],
      [8, 3, 2, 7, 5, 9, 6, 1, 4],
      [9, 6, 5, 3, 1, 4, 8, 7, 2]],
     [[[5, 7], [3, 5, 7], [2, 3, 7], [0, 2, 6], [0, 2], [1], [4, 7], [0, 4, 6], [0, 3, 4, 5]],
      [[1, 2], [5, 6, 7], [0, 8], [3, 6, 7], [0, 2], [7], [0, 5, 7], [0, 4, 8], [0, 1, 3, 8]],
      [[5, 6], [0, 1, 3, 8], [1, 2, 7], [0, 1, 6], [2, 3, 5], [7], [2, 3], [5], [5, 6, 8]],
      [[2, 4, 7], [3, 8], [1, 3], [4, 8], [0, 2, 5, 7], [7], [2, 6, 7], [0, 2, 7], [1, 2, 5]],
      [[7], [2, 3, 7], [0, 4], [2, 5, 6, 8], [1, 5, 8], [2], [2, 3], [3, 6, 8], [1, 3, 7, 8]],
      [[0, 3, 7], [2, 3, 5], [0, 4, 6], [5, 8], [1, 6, 8], [2], [2, 3, 5], [6, 8], [5, 7, 8]],
      [[1, 8], [0, 5], [4, 6, 8], [1, 3], [2, 3, 7, 8], [1, 5], [6, 7], [3, 4, 8], [0, 4, 5]],
      [[0, 7], [1, 2, 8], [0, 4, 5, 6], [8], [1, 3, 4, 8], [5], [1, 3], [5, 7], [1, 4, 7, 8]],
      [[7], [3, 8], [0, 1, 4], [0, 2, 3, 4, 8], [0, 4, 7], [4, 5, 8], [5, 8], [0, 1], [0, 4]],
      [[1, 5, 6, 7], [4, 7], [2, 5], [1, 6, 7], [1, 8], [1, 5, 7], [4, 6], [2, 3, 4], [5, 6]]]],
    [[[1, 9, 6, 3, 2, 5, 4, 7, 8],
      [2, 4, 7, 8, 6, 1, 5, 9, 3],
      [3, 8, 5, 4, 7, 9, 1, 6, 2],
      [4, 1, 8, 6, 9, 3, 2, 5, 7],
      [5, 2, 9, 7, 4, 8, 3, 1, 6],
      [6, 7, 3, 1, 5, 2, 8, 4, 9],
      [7, 6, 2, 5, 8, 4, 9, 3, 1],
      [8, 3, 4, 9, 1, 6, 7, 2, 5],
      [9, 5, 1, 2, 3, 7, 6, 8, 4]],
     [[[1, 5], [2, 4, 8], [1, 3, 4, 7], [0, 1, 8], [5], [0, 5, 6], [6, 7, 8], [5], [3, 8]],
      [[0, 7, 8], [0, 7], [3, 7], [1, 3, 6], [0, 6], [0, 5, 6], [2, 4, 8], [1, 2, 5], [3, 5]],
      [[4, 8], [1], [0, 2, 4, 6], [1, 4, 5, 7], [3, 4], [0, 6, 7], [2, 3, 6], [0, 1, 5], [7]],
      [[2, 6, 8], [0, 1, 5], [4], [2, 4, 8], [0, 1, 6], [1, 3, 5], [3, 4, 6], [1, 7], [4, 6]],
      [[0, 3, 8], [0, 3, 7], [4, 7], [2, 3], [0, 2, 5], [5], [1, 3, 6, 7], [1, 4, 8], [0, 5]],
      [[7], [2, 5, 8], [1, 6, 7], [3, 4], [1, 4], [1, 2, 5, 7], [1, 6], [0, 8], [0, 4, 5, 8]],
      [[1, 3], [0, 4], [1, 6, 8], [0, 1, 6], [1, 2, 4, 6, 8], [1, 4, 7], [7], [8], [2, 5, 7]],
      [[5, 8], [0, 2, 7], [1, 3], [0, 4], [1, 5, 8], [2, 7], [1, 5, 8], [5, 6, 8], [1, 4, 7]],
      [[1, 3], [0, 1, 4, 6, 7], [6], [], [2, 3, 5], [0, 5, 6, 7, 8], [5], [0, 6, 8], [2, 4]],
      [[4, 8], [1], [0, 3, 7], [2, 4], [6, 8], [0, 4, 5], [2, 4, 8], [1, 5, 6, 8], [0, 2, 6]]]],
    [[[1, 5, 6, 2, 7, 8, 9, 4, 3],
      [2, 4, 8, 3, 9, 1, 7, 6, 5],
      [3, 9, 7, 5, 6, 4, 2, 1, 8],
      [4, 2, 9, 1, 5, 7, 3, 8, 6],
      [5, 7, 3, 6, 8, 2, 4, 9, 1],
      [6, 8, 1, 4, 3, 9, 5, 7, 2],
      [7, 6, 5, 8, 4, 3, 1, 2, 9],
      [8, 1, 4, 9, 2, 5, 6, 3, 7],
      [9, 3, 2, 7, 1, 6, 8, 5, 4]],
     [[[2, 3], [4, 5, 6, 8], [0, 4, 6], [1, 6], [0, 7, 8], [1, 3], [1, 4], [0, 5], [0, 4, 7]],
      [[3, 6, 8], [0, 2], [1, 2, 7], [3, 4], [1, 4], [0, 8], [2, 4], [0, 3, 6, 8], [1, 2, 8]],
      [[3, 4], [0, 4, 8], [7], [0, 1, 2, 8], [6], [2, 4], [2, 5, 8], [1, 2, 5, 7], [0, 6]],
      [[6], [7, 8], [2, 5], [1, 3, 4], [4, 7], [0, 1, 7], [1, 5, 6], [0, 7, 8], [0, 2, 5]],
      [[0, 2, 6], [1, 6], [3, 4, 5, 6], [3, 5, 8], [0, 2], [1, 4], [6, 7, 8], [5], [0, 2, 5]],
      [[0, 4, 7], [0, 7], [1, 6], [3, 6], [5, 6, 7], [1, 2, 3], [0, 2, 5], [3, 6, 7], [2]],
      [[0, 4, 7], [3, 8], [0, 3, 6, 7], [0, 2, 6], [3, 5], [1, 2], [2, 4], [6, 8], [0, 4, 5]],
      [[2, 4, 6, 7], [3], [5, 8], [0, 2, 8], [0, 4, 5], [5, 6], [0, 2, 6], [1, 6, 8], [1, 5]],
      [[2, 3, 6, 8], [], [3, 4, 6], [1, 5], [2, 5, 8], [1, 3, 6], [0, 5], [2, 6, 8], [0, 6, 7]],
      [[2, 8], [0, 2, 4, 5], [0, 3], [2, 4, 7], [1, 7, 8], [3], [0, 7], [1, 6], [2, 5, 6, 8]]]]];

function randomEntry(xs) {
    rand = Math.random();
    entry = Math.floor(rand * xs.length);
    return xs[entry];
}

function randomEntries(xs, count) {
    var chosenMap = {};
    var chosen = [];
    while (chosen.length < count) {
        var entry = randomEntry(xs);
        if (!(entry in chosenMap)) {
            chosenMap[entry] = true;
            chosen = chosen.concat([entry]);
        }
    }
    return chosen;
}

function randomSequenceOfThree() {
    return randomEntry([
        [0, 1, 2],
        [0, 2, 1],
        [1, 0, 2],
        [1, 2, 0],
        [2, 0, 1],
        [2, 1, 0]]);
}

function randomSequenceFromOneToNine() {
    var indices = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    var result = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    for (var i = 0; i < 8; i++) {
        var swap = randomEntry(indices);
        var temp = result[i];
        result[i] = result[swap];
        result[swap] = temp;
        indices.shift();
    }
    return result;
}

function buildGame(startingBoard, mask) {
    var valueOrder = randomSequenceFromOneToNine();
    var game = [];
    for (var rowNum = 0; rowNum < 9; rowNum++) {
        var row = []
        for (var colNum = 0; colNum < 9; colNum++) {
            var cell = {
                "value": valueOrder[startingBoard[rowNum][colNum] - 1],
                "revealed": false,
                "display": "",
                "notes": {}
            }
            row = row.concat([cell]);
        }
        for (var idx in mask[rowNum]) {
            var colNum = mask[rowNum][idx];
            var cell = row[colNum];
            cell["revealed"] = true;
            cell["display"] = "" + cell["value"];
        }
        game = game.concat([row]);
    }
    return game;
}

function randomGamePrototype() {
    var gameMasksPair = randomEntry(startingBoards);
    var startingBoard = gameMasksPair[0];
    var mask = randomEntry(gameMasksPair[1]);
    return buildGame(startingBoard, mask);
}

function generateRandomNestedOrder() {
    var generatedOrder = [];
    var outerOrder = randomSequenceOfThree();
    for (var outerOrderIndex in outerOrder) {
        var outerVal = outerOrder[outerOrderIndex];
        var innerOrder = randomSequenceOfThree();
        for (var innerOrderIndex in innerOrder) {
            var innerVal = innerOrder[innerOrderIndex];
            generatedOrder = generatedOrder.concat([(outerVal * 3) + innerVal]);
        }
    }
    return generatedOrder;
}

function shuffle(game) {
    var rowOrder = generateRandomNestedOrder();
    var colOrder = generateRandomNestedOrder();
    var shuffledGame = [];
    for (var rowIndex in rowOrder) {
        var rowNum = rowOrder[rowIndex];
        var shuffledRow = [];
        for (var colIndex in colOrder) {
            var colNum = colOrder[colIndex];
            shuffledRow = shuffledRow.concat([game[rowNum][colNum]]);
        }
        shuffledGame = shuffledGame.concat([shuffledRow]);
    }
    return shuffledGame;
}

function newGame() {
    return shuffle(randomGamePrototype());
}

function disjointCells(row, col) {
    var disjoint = [];
    for (var num = 0; num < 9; num++) {
        if (num != row) {
            disjoint = disjoint.concat([[num, col]]);
        }
        if (num != col) {
            disjoint = disjoint.concat([[row, num]]);
        }
    }
    var outerRow = Math.floor(row / 3);
    var outerCol = Math.floor(col / 3);
    for (var innerRow = 0; innerRow < 3; innerRow++) {
        var rowNum = (outerRow * 3) + innerRow;
        if (rowNum != row) {
            for (var innerCol = 0; innerCol < 3; innerCol++) {
                var colNum = (outerCol * 3) + innerCol;
                if (colNum != col) {
                    disjoint = disjoint.concat([[rowNum, colNum]]);
                }
            }
        }
    }
    return disjoint;
}

var currGame = null;
var notesMode = false;
var gameSolved = false;
var currRow = -1;
var currCol = -1;
function positionUnset() {
    if (currRow < 0) {
        return true;
    }
    if (currCol < 0) {
        return true;
    }
    return false;
}

function hiddenCells(game) {
    var hidden = [];
    for (var row = 0; row < 9; row++) {
        for (var col = 0; col < 9; col++) {
            if (!currGame[row][col]["revealed"]) {
                hidden = hidden.concat([[row, col]]);
            }
        }
    }
    return hidden;
}

function hasConflict(row, col) {
    var cell = currGame[row][col];
    var disjoint = disjointCells(row, col);
    for (var idx in disjoint) {
        var disjointCell = disjoint[idx];
        var otherRow = disjointCell[0];
        var otherCol = disjointCell[1];
        var otherCell = currGame[otherRow][otherCol];
        if (otherCell["display"] == cell["display"]) {
            return true;
        }
    }
    return false;
}

function redrawCell(row, col) {
    if ((row < 0) || (col < 0)) {
        return null;
    }
    var cell = currGame[row][col];
    for (var noteNum = 1; noteNum < 10; noteNum++) {
        var noteElem = document.getElementById("note-"+row+"-"+col+"-"+noteNum);
        noteElem.style.visibility = "hidden";
    }
    var cellElem = document.getElementById("contents-"+row+"-"+col);
    cellElem.innerHTML = cell["display"];
    cellElem.style.color = "black";
    if (cell["revealed"]) {
        cellElem.style.backgroundColor = "lightgrey";
        return cell;
    }
    if ((currRow == row) && (currCol == col)) {
        cellElem.style.backgroundColor = "#FFFF90";
    } else {
        cellElem.style.backgroundColor = "white";
    }
    if (cell["display"] == "") {
        for (var noteNum in cell["notes"]) {
            var noteElem = document.getElementById("note-"+row+"-"+col+"-"+noteNum);
            noteElem.style.visibility = "visible";
        }
    } else if (hasConflict(row, col)) {
        cellElem.style.color = "red";
    }
    var disjoint = disjointCells(row, col);
    for (var idx in disjoint) {
        var disjointCell = disjoint[idx];
        var otherRow = disjointCell[0];
        var otherCol = disjointCell[1];
        var otherCell = currGame[otherRow][otherCol];
        if (!otherCell["revealed"]) {
            var otherElem = document.getElementById("contents-"+otherRow+"-"+otherCol);
            if (hasConflict(otherRow, otherCol)) {
                otherElem.style.color = "red";
            } else {
                otherElem.style.color = "black";
            }
        }
    }
    return cell;
}

function clickHandler(row,col) {
    if (gameSolved) {
        return;
    }
    if ((currRow >= 0) && (currCol >= 0)) {
        var oldElem = document.getElementById("contents-"+currRow+"-"+currCol);
        if (!currGame[currRow][currCol]["revealed"]) {
            oldElem.style.backgroundColor = "white";
        }
    }

    currRow = row;
    currCol = col;
    var newElem = document.getElementById("contents-"+currRow+"-"+currCol);
    if (!currGame[currRow][currCol]["revealed"]) {
        newElem.style.backgroundColor = "#FFFF90";
    }
}

function isSolved(game) {
    for (var row = 0; row < 9; row++) {
        for (var col = 0; col < 9; col++) {
            var cell = game[row][col];
            if (cell["display"] != "" + cell["value"]) {
                return false;
            }
        }
    }
    return true;
}

function finalizeGame() {
    for (var row = 0; row < 9; row++) {
        for (var col = 0; col < 9; col++) {
            var cell = currGame[row][col];
            cell["revealed"] = true;
        }
    }
    localStorage.setItem(localStorageKey, JSON.stringify(currGame));
    reload();
    return true;
}

function keyPressed(key) {
    if (positionUnset() || gameSolved) {
        return;
    }
    var cell = currGame[currRow][currCol];
    if (cell["revealed"]) {
        return;
    }
    if (notesMode) {
        notes = cell["notes"];
        if (key in notes) {
            delete(notes[key]);
        } else {
            notes[key] = true;
        }
    } else {
        cell["display"] = "" + key;
    }
    localStorage.setItem(localStorageKey, JSON.stringify(currGame));
    if ((!notesMode) && isSolved(currGame)) {
        gameSolved = true;
        finalizeGame();
    } else {
        redrawCell(currRow, currCol);
        reloadButtons();
    }
}

function toggleNotes() {
    notesMode = !notesMode;
    if (notesMode) {
        document.getElementById("notesButton").style.color = "blue";
    } else {
        document.getElementById("notesButton").style.color = "black";
    }
}

function erase() {
    if (positionUnset() || gameSolved) {
        return;
    }
    var cell = currGame[currRow][currCol];
    if (cell["revealed"]) {
        return;
    }
    if (notesMode) {
        if (cell["display"] == "") {
            cell["notes"] = {};
        }
    } else {
        cell["display"] = "";
    }
    localStorage.setItem(localStorageKey, JSON.stringify(currGame));
    redrawCell(currRow, currCol);
    reloadButtons();
}

function reloadButtons() {
    if (gameSolved) {
        document.getElementById("notesButton").disabled = true;
        document.getElementById("simplifyButton").disabled = true;
        document.getElementById("eraseButton").disabled = true;
    } else {
        document.getElementById("notesButton").disabled = false;
        document.getElementById("simplifyButton").disabled = false;
        document.getElementById("eraseButton").disabled = false;
        for (var i = 1; i <= 9; i++) {
            document.getElementById("b" + i).disabled = false;
        }
    }
    var enteredCounts = {};
    for (var row = 0; row < 9; row++) {
        for (var col = 0; col < 9; col++) {
            var cell = redrawCell(row, col);
            if (cell["display"] != "") {
                var buttonId = "b" + cell["display"];
                if (buttonId in enteredCounts) {
                    enteredCounts[buttonId] = enteredCounts[buttonId] + 1;
                } else {
                    enteredCounts[buttonId] = 1;
                }
            }
        }
    }
    for (var buttonId in enteredCounts) {
        if (enteredCounts[buttonId] == 9) {
            document.getElementById(buttonId).disabled = true;
        } else {
            document.getElementById(buttonId).disabled = false;
        }
    }
}

function reload() {
    if (!localStorage.getItem(localStorageKey)) {
        localStorage.setItem(localStorageKey, JSON.stringify(newGame()));
    }
    currGame = JSON.parse(localStorage.getItem(localStorageKey));
    if (isSolved(currGame)) {
        gameSolved = true;
        notesMode = false;
        document.getElementById("notesButton").style.color = "black";
    } else {
        gameSolved = false;
    }
    reloadButtons();
}

function simplify() {
    var hidden = hiddenCells(currGame);
    if (hidden.length < 40) {
        return;
    }
    if (!window.confirm("Simplify game by revealing 5 additional answers?")) {
        return;
    }
    var chosenCells = randomEntries(hidden, 5);
    for (var chosenIndex in chosenCells) {
        var cellPos = chosenCells[chosenIndex];
        var row = cellPos[0];
        var col = cellPos[1];
        var cell = currGame[row][col];
        console.log(cell);
        cell["revealed"] = true;
        cell["display"] = "" + cell["value"];
    }
    localStorage.setItem(localStorageKey, JSON.stringify(currGame));
    reload();
}

function loadNewGame() {
    if ((!gameSolved) && (!window.confirm("Quit this game and start a new one?"))) {
        return;
    }
    localStorage.setItem(localStorageKey, JSON.stringify(newGame()));
    reload();
}

function privacyPolicy() {
    window.alert(
        "This app does not collect any personal data.\n" +
            "\n" +
            "If you installed this app from an app store, " +
            "such as the Google Play Store, then refer to " +
            "that app store's privacy policy for any " +
            "information they collect.");
}
